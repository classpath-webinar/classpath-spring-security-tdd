package com.classpath.itemmgmt.service;

import com.classpath.itemmgmt.model.Item;
import com.classpath.itemmgmt.repository.ItemRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service("itemService")
public class ItemServiceImpl implements ItemService {

    //LoggerFactory.getLogger(ItemServiceImpl.class);
    private final ItemRepository itemRepository;

    public ItemServiceImpl(ItemRepository itemRepository){
        this.itemRepository = itemRepository;
    }


    @Override
    public Item save(Item item) {
        System.out.println(item.getItemId());
        System.out.println(item.getItemName());
      //  log.info("Came inside the service method with argument - price - {}, desc - {} ", item.getPrice(), item.getDescription());
        Item savedItem = this.itemRepository.save(item);
        return savedItem;

    }

    @Override
    public List<Item> listAll() {
        return this.itemRepository.findAll();
    }

    @Override
    public Item findById(long itemId) {
        return this.itemRepository.findById(itemId).orElseThrow(()-> new IllegalArgumentException("Invalid Item Id"));
    }

    @Override
    public Item update(long itemId, Item item) {

               Item itemToBeSaved = this.itemRepository
                                .findById(itemId)
                                .map(rItem  -> {
                                    rItem.setItemName(item.getItemName());
                                    rItem.setDescription(item.getDescription());
                                    rItem.setPrice(item.getPrice());
                                    return rItem;
                                })
                                .orElseGet(() -> item);

        return this.itemRepository.save(itemToBeSaved);
    }

    @Override
    public void deleteItemById(long itemId) {
        this.itemRepository.deleteById(itemId);
    }
}