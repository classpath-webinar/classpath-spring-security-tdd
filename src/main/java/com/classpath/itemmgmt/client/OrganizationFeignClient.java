package com.classpath.itemmgmt.client;

import com.classpath.itemmgmt.model.Organization;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface OrganizationFeignClient {

    @RequestMapping(
            value = "/api/v1/organization/{id}",
            method = RequestMethod.GET,
            consumes = "application/json"
    )
    Organization getOrganizationById(@PathVariable("id") long organizationId);
}